from django.shortcuts import render, redirect
from .models import Product, CartItem
from django.contrib.auth.models import User

def home(request):
    products = Product.objects.all()
    return render(request, 'home.html', {'products': products})

def add_to_cart(request, product_id):
    if request.user.is_authenticated:
        product = Product.objects.get(pk=product_id)
        cart_item, created = CartItem.objects.get_or_create(user=request.user, product=product)
        if not created:
            cart_item.quantity += 1
            cart_item.save()
    return redirect('home')

def cart(request):
    cart_items = CartItem.objects.filter(user=request.user)
    total_cost = sum(item.product.price * item.quantity for item in cart_items)
    return render(request, 'cart.html', {'cart_items': cart_items, 'total_cost': total_cost})

def increase_quantity(request, cart_item_id):
    cart_item = CartItem.objects.get(pk=cart_item_id)
    if cart_item.quantity < cart_item.product.stock_quantity:
        cart_item.quantity += 1
        cart_item.save()
    return redirect('cart')

def decrease_quantity(request, cart_item_id):
    cart_item = CartItem.objects.get(pk=cart_item_id)
    if cart_item.quantity > 1:
        cart_item.quantity -= 1
        cart_item.save()
    return redirect('cart')

def checkout(request):
    cart_items = CartItem.objects.filter(user=request.user)
    for item in cart_items:
        item.product.stock_quantity -= item.quantity
        item.product.save()
    cart_items.delete()
    return redirect('home')
