from django.urls import path
from clickshop.views import home, add_to_cart, cart, increase_quantity, decrease_quantity, checkout

urlpatterns = [
    path('', home, name='home'),
    path('add_to_cart/<int:product_id>/', add_to_cart, name='add_to_cart'),
    path('cart/', cart, name='cart'),
    path('increase_quantity/<int:cart_item_id>/', increase_quantity, name='increase_quantity'),
    path('decrease_quantity/<int:cart_item_id>/', decrease_quantity, name='decrease_quantity'),
    path('checkout/', checkout, name='checkout'),
]
